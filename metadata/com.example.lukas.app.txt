Categories:Navigation,Office
License:GPLv3
Web Site:https://github.com/kas70/SAnd/blob/HEAD/README.md
Source Code:https://github.com/kas70/SAnd
Issue Tracker:https://github.com/kas70/SAnd/issues

Auto Name:SAnd
Summary:Show orientation, height and airpressure
Description:
Use your phones sensors (barometer and compass) to show your current
orientation, height and air pressure.
.

Repo Type:git
Repo:https://github.com/kas70/SAnd

Build:1.0,1
    commit=b90c5695a6b351e111e51a8901b4ec5a48161a68
    subdir=app
    gradle=yes
    rm=app/libs/*

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1

